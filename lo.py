# -*- coding: utf-8 -*-
import re
import urllib.request
from slack.web.classes import extract_json
from slack.web.classes.blocks import *
from bs4 import BeautifulSoup

from flask import Flask
from slack import WebClient
from slackeventsapi import SlackEventAdapter

SLACK_TOKEN = "xoxb-689670380069-689660313040-RSIgcvLS67hBqATzHx4MVn3g"
SLACK_SIGNING_SECRET = "521bb9f484e6bf9e0f2cb3465958d9cb"

app = Flask(__name__)
# /listening 으로 슬랙 이벤트를 받습니다.
slack_events_adaptor = SlackEventAdapter(SLACK_SIGNING_SECRET, "/listening", app)
slack_web_client = WebClient(token=SLACK_TOKEN)


# 앨범 정보 출력
def _crawl_music_search(text):

    save = text.split()[-1]
    save_song_title = text.split()[1:-1]
    save_song_title = " ".join(save_song_title)

    url = "https://music.naver.com/listen/top100.nhn?domain=TOTAL&duration=1d&page=1"
    req = urllib.request.Request(url)
    source_code = urllib.request.urlopen(url).read()
    soup = BeautifulSoup(source_code, "html.parser")

    save_title = []
    save_singer = []
    save_link = []

    for title_ in soup.find_all("a", class_="_title"):
        save_title.append(title_.get_text().strip())
    for singer_ in soup.find_all("td", class_="_artist")[1:]:
        save_singer.append(singer_.get_text().strip())
    for link_ in soup.find_all("a", class_="thumb"):
        if 'href' in link_.attrs:
            save_link.append(link_.attrs['href'])

    if "앨범정보" in save:
        for a in range(0, len(save_title)):
            if str(save_song_title) in save_title[a]:
                url = "https://music.naver.com/" + save_link[a]

        req = urllib.request.Request(url)
        source_code = urllib.request.urlopen(url).read()
        soup = BeautifulSoup(source_code, "html.parser")

        title = []
        singer = []
        date = []
        genre = []
        song_list = []
        message_ = []

        for title_ in soup.find_all("div", class_="info_txt"):
            title.append(title_.find("h2").get_text().strip())
        for singer_ in soup.find("dl", class_="desc").find("a"):
            singer.append(singer_.strip())
        for date_ in soup.find("dl", class_="desc").find_all("dd")[2]:
            date.append(str(date_.strip()))
        for genre_ in soup.find("dl", class_="desc").find_all("dd")[1]:
            genre.append(genre_.strip())
        for song_list_ in soup.find("ol", class_="song_info_ol").find_all("span", class_="tit"):
            song_list.append(song_list_.get_text().strip())

        message_.append("앨범명 : " + title[0] + "  가수명 : " + singer[0] + "  발매일 : " + date[0] + "  장르 : " + genre[0] + "\n곡 리스트")

        for i in range(1, len(song_list) + 1):
            message_.append(str(i) + ". " + song_list[i - 1])

        message_.append("\n★아래의 형식과 같이 입력이 가능합니다. 정확하게 입력해주세요★" +
                      "\n@<봇 이름> 노래제목 앨범정보" +
                      "\n@<봇 이름> 노래제목 가사" +
                      "\n@<봇 이름> 노래제목 유튜브" +
                      "\n@<봇 이름> 장르별순위 장르 EX) 발라드, 댄스, 힙합, 팝, 알앤비")

        message = "\n".join(message_)

    return message

# 가사 검색
def _crawl_music_lyrics(text):

    message = []
    save = text.split()[-1]
    save_song_title = text.split()[1:-1]
    save_song_title = " ".join(save_song_title)

    url = "https://music.naver.com/listen/top100.nhn?domain=TOTAL&duration=1d&page=1"
    req = urllib.request.Request(url)
    source_code = urllib.request.urlopen(url).read()
    soup = BeautifulSoup(source_code, "html.parser")

    save_title = []
    save_lyrics = []

    for title_ in soup.find_all("a", class_="_title"):
        save_title.append(title_.get_text().strip())
    for lyrics_ in soup.find_all("a", class_="title"):
        if 'href' in lyrics_.attrs:
            save_lyrics.append(lyrics_.attrs['href'].replace("#", ""))

    if "가사" in save:
        for a in range(0, len(save_title)):
            if str(save_song_title) in save_title[a]:
                url = "https://music.naver.com/lyric/index.nhn?trackId="+str(save_lyrics[a])
        req = urllib.request.Request(url)
        source_code = urllib.request.urlopen(url).read()
        soup = BeautifulSoup(source_code, "html.parser")

        lyrics = []

        for lyrics_ in str(soup.find_all("div", class_="show_lyrics")).split(">"):
            lyrics.append(lyrics_.replace("<br/", "").replace('[<div class="show_lyrics" id="lyricText"', "").replace("</div", "").replace("]", ""))

        lyrics.append("\n★아래의 형식과 같이 입력이 가능합니다. 정확하게 입력해주세요★" +
                      "\n@<봇 이름> 노래제목 앨범정보" +
                      "\n@<봇 이름> 노래제목 가사" +
                      "\n@<봇 이름> 노래제목 유튜브" +
                      "\n@<봇 이름> 장르별순위 장르 EX) 발라드, 댄스, 힙합, 팝, 알앤비")

        message = "\n".join(lyrics)

    return message

# 처음 시작 화면

def _crawl_music_chart(text):

    url = "https://music.naver.com/listen/top100.nhn?domain=TOTAL&duration=1d&page=1"
    req = urllib.request.Request(url)
    source_code = urllib.request.urlopen(url).read()
    soup = BeautifulSoup(source_code, "html.parser")

    title = []
    singer = []
    message_ = []

    for title_ in soup.find_all("a", class_="_title"):
        title.append(title_.get_text().strip())
    for singer_ in soup.find_all("td", class_="_artist")[1:]:
        singer.append(singer_.get_text().strip())

    for i in range(1, len(title)+1):
        message_.append(str(i) + "위 : " + title[i-1] + " / " + singer[i-1])

    message_.append("\n★아래의 형식과 같이 입력이 가능합니다. 정확하게 입력해주세요★" +
                    "\n@<봇 이름> 노래제목 앨범정보" +
                    "\n@<봇 이름> 노래제목 가사" +
                    "\n@<봇 이름> 노래제목 유튜브" +
                    "\n@<봇 이름> 장르별순위 장르 EX) 발라드, 댄스, 힙합, 팝, 알앤비")

    message = "\n".join(message_)

    return message

# 유튜브 검색
def _crawl_music_youtube(text):

    save = text.split()[-1]
    save_tube = text.split()[1:-1]

    message_ = []
    message = []

    url = "https://www.youtube.com/results?search_query="
    if "유튜브" in save:
        if len(save_tube) != 0:
            for a in range(0, len(save_tube)):
                if a == 0:
                    url += str(save_tube[a])
                else:
                    url += "+" + str(save_tube[a])
        else:
            url += str(save_tube)

    message_.append(str(url))
    message_.append("\n\n★아래의 형식과 같이 입력이 가능합니다. 정확하게 입력해주세요★" +
                    "\n@<봇 이름> 노래제목 앨범정보" +
                    "\n@<봇 이름> 노래제목 가사" +
                    "\n@<봇 이름> 노래제목 유튜브" +
                    "\n@<봇 이름> 장르별순위 장르 EX) 발라드, 댄스, 힙합, 팝, 알앤비")

    message = " ".join(message_)
    return message


# 장르별 순위
def _crawl_music_rank(text):
    message = []
    genre = ["K01", "K02", "K03", "P01", "P04"]
    save_genre = text.split()[2:-1]
    print(save_genre)
    save_link = text.split()[-1]
    if "장르별순위" in text:
        for a in range(0, len(genre)):
            if save_link == genre[a]:
                url = "https://music.naver.com/listen/genre/top100.nhn?domain=OVERSEA&genre=" + genre[a]
        req = urllib.request.Request(url)
        source_code = urllib.request.urlopen(url).read()
        soup = BeautifulSoup(source_code, "html.parser")

        title = []
        singer = []
        message_ = []

        for title_ in soup.find_all("a", class_="_title"):
            title.append(title_.get_text().strip())
        for singer_ in soup.find_all("td", class_="_artist")[1:]:
            singer.append(singer_.get_text().strip())
        for i in range(1, len(title) + 1):
            message_.append(str(i) + "위 : " + title[i - 1] + " / " + singer[i - 1] + "\n")

        message_.append("\n★아래의 형식과 같이 입력이 가능합니다. 정확하게 입력해주세요★" +
                        "\n@<봇 이름> 노래제목 앨범정보" +
                        "\n@<봇 이름> 노래제목 가사" +
                        "\n@<봇 이름> 노래제목 유튜브" +
                        "\n@<봇 이름> 장르별순위 장르 EX) 발라드, 댄스, 힙합, 팝, 알앤비")
        message = " ".join(message_)

    return message


def _crawl(text):
    if not "영화" in text:
        return "`@<봇이름> 영화` 과 같이 멘션해주세요."

    message=['현재 예매 순위를 보여드릴게요!']
    url = 'https://movie.naver.com/movie/running/current.nhn?view=list&tab=normal&order=reserve'
    source_code = urllib.request.urlopen(url).read()
    soup = BeautifulSoup(source_code, "html.parser")

    box_office = []
    line = []
    for data in soup.find_all('dt', class_='tit'):
        box_office.append(data.find('a').get_text())
    for i in range(1, len(box_office)+1):
        line.append(str(i)+'위:' + box_office[i-1])
    question = ['궁금한 영화를 맨션과 함께, {영화제목} {줄거리 or 출연배우 or 감독 or 예매링크} 으로 입력하세요']
    message = message+line+question
    return u'\n'.join(message)
    # url = 'https://music.bugs.co.kr/chart/track/realtime/total?wl_ref=M_contents_03_01'
    # source_code = urllib.request.urlopen(url).read()
    # soup = BeautifulSoup(source_code, "html.parser")
    #
    # # 여기에 함수를 구현해봅시다.
    # title = []
    # singer = []
    # message = []
    # for data in soup.find_all('p', class_='title'):
    #     title.append(data.get_text().strip())
    # for data in soup.find_all('p', class_='artist'):
    #     singer.append(data.get_text().strip().split('\n\n\r\n')[0])
    #
    # for i in range(1, 11):
    #     message.append(str(i) + '위:' + str(title[i] + '/' + str(singer[i])))
    # return u'\n'.join(message)
def _crawl_story(text):
    url = 'https://movie.naver.com/movie/running/current.nhn?view=list&tab=normal&order=reserve'
    source_code = urllib.request.urlopen(url).read()
    soup = BeautifulSoup(source_code, "html.parser")
    titles=[]
    i = 0
    link = []
    for title in soup.find_all('dt', class_='tit'):
        titles.append(title.find('a').get_text())

    for title in titles:
        if title == text:
            link.append(soup.find_all('dt',class_='tit')[i].find('a')['href'])
        i += 1
    #print(link)
    url = 'https://movie.naver.com' + link[0]
    source_code = urllib.request.urlopen(url).read()
    soup2 = BeautifulSoup(source_code, "html.parser")
    story = []
    for data in soup2.find_all('p', class_='con_tx'):
        story.append(data.get_text())
    #print(story)
    question = ['\n궁금한 영화를 맨션과 함께, {영화제목} {줄거리 or 출연배우 or 감독 or 예매링크} 으로 입력하세요']
    story = story+question
    return u'\n'.join(story)

def _crawl_actor(text):
    url = 'https://movie.naver.com/movie/running/current.nhn?view=list&tab=normal&order=reserve'
    source_code = urllib.request.urlopen(url).read()
    soup = BeautifulSoup(source_code, "html.parser")
    titles = []
    i = 0
    link = []
    for title in soup.find_all('dt', class_='tit'):
        titles.append(title.find('a').get_text())

    for title in titles:
        if title == text:
            link.append(soup.find_all('dt', class_='tit')[i].find('a')['href'])
        i += 1
    print(link)
    url = 'https://movie.naver.com' + link[0]
    source_code = urllib.request.urlopen(url).read()
    soup2 = BeautifulSoup(source_code, "html.parser")
    actors=[]

    for data in soup2.find_all('a', class_='tx_people')[1:]:
        actors.append(data.get_text())
        question = ['\n궁금한 영화를 맨션과 함께, {영화제목} {줄거리 or 출연배우 or 감독 or 예매링크} 으로 입력하세요']
    actors+=question
    return u'\n'.join(actors)

def _crawl_director(text):
    url = 'https://movie.naver.com/movie/running/current.nhn?view=list&tab=normal&order=reserve'
    source_code = urllib.request.urlopen(url).read()
    soup = BeautifulSoup(source_code, "html.parser")
    titles = []
    i = 0
    link = []
    for title in soup.find_all('dt', class_='tit'):
        titles.append(title.find('a').get_text())

    for title in titles:
        if title == text:
            link.append(soup.find_all('dt', class_='tit')[i].find('a')['href'])
        i += 1
    #print(link)
    url = 'https://movie.naver.com' + link[0]
    source_code = urllib.request.urlopen(url).read()
    soup2 = BeautifulSoup(source_code, "html.parser")
    directors = []

    directors.append(soup2.find('a', class_='tx_people').get_text())
    question = ['\n궁금한 영화를 맨션과 함께, {영화제목} {줄거리 or 출연배우 or 감독 or 예매링크} 으로 입력하세요']
    directors+=question
    return u'\n'.join(directors)

def _crawl_booking(text):
    message=[]
    url = 'https://movie.naver.com/movie/running/current.nhn?view=list&tab=normal&order=reserve'
    source_code = urllib.request.urlopen(url).read()
    soup = BeautifulSoup(source_code, "html.parser")
    titles = []
    i = 0
    link = []
    for title in soup.find_all('dt', class_='tit'):
        titles.append(title.find('a').get_text())

    for title in titles:
        if title == text:
            link.append(soup.find_all('div', class_='btn_area')[i].find('a')['href'])
        i += 1
    #print(link)
    url = 'https://movie.naver.com' + link[0]
    message.append(url)
    question = ['\n궁금한 영화를 맨션과 함께, {영화제목} {줄거리 or 출연배우 or 감독 or 예매링크} 으로 입력하세요']
    message+=question
    return u'\n'.join(message)

def _crawl_drama_ranking():
    message = ['현재 드라마 검색 순위 입니다.']
    url = 'https://search.naver.com/search.naver?sm=tab_hty.top&where=nexearch&query=%EB%93%9C%EB%9D%BC%EB%A7%88&oquery=drama&tqi=UfkPYsp0J1sssS6xbRlssssst68-176765'
    source_code = urllib.request.urlopen(url).read()
    soup = BeautifulSoup(source_code, "html.parser")

    titles=[]

    for title in soup.find_all('span', class_='tit')[:10]:
        titles.append(title.get_text())
    sentence=[]
    for i in range(10):
        sentence.append(str(i+1)+'위:'+ titles[i])

    message2 = ['\n맨션과 함께 {드라마 검색순위 or 드라마 시청률 순위}라고 입력하세요.']
    full_message=message+sentence+message2

    return u'\n'.join(full_message)

def _crawl_drama_see_ranking():
    message = ['현재 방영중인 드라마의 시청률 순위 입니다.']
    url = 'https://search.naver.com/search.naver?where=nexearch&sm=tab_etc&mra=blUw&query=%EC%A3%BC%EA%B0%84%EB%93%9C%EB%9D%BC%EB%A7%88%20%EC%8B%9C%EC%B2%AD%EB%A5%A0'
    source_code = urllib.request.urlopen(url).read()
    soup = BeautifulSoup(source_code, "html.parser")

    titles=[]
    real_titles=[]
    ranking_title=[]
    for title in soup.find_all('td'):
        for ti in title.find_all('p'):
            titles.append(str(ti.find('a')).split('>'))

    for i in range(titles.count(['None'])):
        titles.remove(['None'])
    for i in range(0, 40, 2):
        if titles[i][-2][:-3] in real_titles:
            real_titles.append(titles[i][-2][:-3]+'(재)')
        else:
            real_titles.append(titles[i][-2][:-3])
    for rate in soup.find_all('p',class_='rate n123'):
        print(rate.get_text())

    for i in range(20):
        ranking_title.append(str(i+1)+'위:'+ real_titles[i])
    last_message=['드라마의 정보를 알고 싶으시다면 맨션과 함께 {드라마이름} {링크} 라고 입력하세요.']
    message2=['\n맨션과 함께 {드라마 검색순위 or 드라마 시청률 순위}라고 입력하세요.']
    full_message = message+ranking_title+message2+last_message
    return u'\n'.join(full_message)

def _crawl_drama_link(text):
    url = 'https://search.naver.com/search.naver?where=nexearch&sm=tab_etc&mra=blUw&query=%EC%A3%BC%EA%B0%84%EB%93%9C%EB%9D%BC%EB%A7%88%20%EC%8B%9C%EC%B2%AD%EB%A5%A0'
    source_code = urllib.request.urlopen(url).read()
    soup = BeautifulSoup(source_code, "html.parser")

    links = []
    for link in soup.find_all('td'):
        for li in link.find_all('p'):
            links.append(str(li.find('a')))
    for i in range(links.count('None')):
        links.remove('None')
    real_links=[]
    for i in range(40):
        real_links.append(links[i].split('nocr'))
    #print(real_links)

    final=[]
    for i in range(0,40,2):
        final.append(real_links[i][0][9:])

    final_links=[]
    for i in range(20):
        final_links.append('https://search.naver.com/search.naver'+final[i])

    titles = []
    real_titles = []
    message=[]
    for title in soup.find_all('td'):
        for ti in title.find_all('p'):
            titles.append(str(ti.find('a')).split('>'))

    for i in range(titles.count(['None'])):
        titles.remove(['None'])
    for i in range(0, 40, 2):
        if titles[i][-2][:-3] in real_titles:
            real_titles.append(titles[i][-2][:-3] + '(재)')
        else:
            real_titles.append(titles[i][-2][:-3])
    #print(real_titles)
    for i in range(20):
        if real_titles[i] == text:
            message.append(final_links[i])

    last_message = ['다른 드라마의 정보를 알고 싶으시다면 맨션과 함께 {드라마이름} {링크} 라고 입력하세요.']
    message2 = ['\n맨션과 함께 {드라마 검색순위 or 드라마 시청률 순위}라고 입력하세요.']
    message=message+message2+last_message
    return u'\n'.join(message)


# 챗봇이 멘션을 받았을 경우
@slack_events_adaptor.on("app_mention")
def app_mentioned(event_data):

    channel = event_data["event"]["channel"]
    text = event_data["event"]["text"]

    text_last = text.split()[-1]

    if text.split()[1] == '영화':
        message = _crawl(text)
        slack_web_client.chat_postMessage(
            channel=channel,
            text=message
        )

    if '줄거리' == text_last:
        text = text.split()[1:-1]
        text = ' '.join(text)
        message = _crawl_story(text)
        slack_web_client.chat_postMessage(
            channel=channel,
            text=message)

    if '출연배우' == text_last:
        text = text.split()[1:-1]
        text = ' '.join(text)
        message = _crawl_actor(text)
        slack_web_client.chat_postMessage(
            channel=channel,
            text=message)

    if '감독' == text_last:
        text = text.split()[1:-1]
        text = ' '.join(text)
        message = _crawl_director(text)
        slack_web_client.chat_postMessage(
            channel=channel,
            text=message)

    if '예매링크' == text_last:
        text = text.split()[1:-1]
        text = ' '.join(text)
        message = _crawl_booking(text)
        slack_web_client.chat_postMessage(
            channel=channel,
            text=message)

    if text_last == '검색순위':
        message = _crawl_drama_ranking()
        slack_web_client.chat_postMessage(
            channel=channel,
            text=message)

    if text_last == '순위':
        message = _crawl_drama_see_ranking()
        slack_web_client.chat_postMessage(
            channel=channel,
            text=message)

    if '링크' == text_last:
        text = text.split()[1:-1]
        text = ' '.join(text)
        message = _crawl_drama_link(text)
        slack_web_client.chat_postMessage(
            channel=channel,
            text=message)


    if text.split()[1] == "노래":
        message = _crawl_music_chart(text)
        slack_web_client.chat_postMessage(
            channel=channel,
            text=message
        )

    elif text.split()[-1] == "앨범정보":

        message = _crawl_music_search(text)
        block1 = ImageBlock(
            image_url="이미지의 URL",
            alt_text="이미지 없음"
        )
        slack_web_client.chat_postMessage(
            channel=channel,
            text=message
        )

    elif text.split()[-1] == "가사":
        message = _crawl_music_lyrics(text)
        slack_web_client.chat_postMessage(
            channel=channel,
            text=message
        )

    elif text.split()[-1] == "유튜브":
        message = _crawl_music_youtube(text)
        slack_web_client.chat_postMessage(
            channel=channel,
            text=message
        )

    elif text.split()[1] == "장르별순위":
        if text.split()[-1] == "발라드":
            text += " K01"
            message = _crawl_music_rank(text)
            slack_web_client.chat_postMessage(
                channel=channel,
                text=message
            )
        elif text.split()[-1] == "댄스":
            text += " K02"
            message = _crawl_music_rank(text)
            slack_web_client.chat_postMessage(
                channel=channel,
                text=message
            )
        elif text.split()[-1] == "힙합":
            text += " K03"
            message = _crawl_music_rank(text)
            slack_web_client.chat_postMessage(
                channel=channel,
                text=message
            )
        elif text.split()[-1] == "팝":
            text += " P01"
            message = _crawl_music_rank(text)
            slack_web_client.chat_postMessage(
                channel=channel,
                text=message
            )
        elif text.split()[-1] == "알앤비":
            text += " P04"
            message = _crawl_music_rank(text)
            slack_web_client.chat_postMessage(
                channel=channel,
                text=message
            )

# / 로 접속하면 서버가 준비되었다고 알려줍니다.
@app.route("/", methods=["GET"])
def index():
    return "<h1>Server is ready.</h1>"


if __name__ == '__main__':
    app.run('127.0.0.1', port=5000)
